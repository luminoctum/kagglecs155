#! /usr/bin/env python2.7
from pylab import *
import pickle, operator, datetime
from sklearn import linear_model
from numpy.random import permutation

# data file has been load into
# wordcount.dat, tfidf.dat, prediction.dat for faster reading

def score_func(test, correct):
    return 1. - float(sum(map(operator.xor, test, correct))) / len(test)

def read_wordcount_file():
    data = genfromtxt('kaggle_train_wc.csv', skip_header = 1, delimiter = ',')
    # remove first column
    predict = data[:, -1]
    data = data[:, 1:-1]
    with open('wordcount.dat', 'w') as file:
        pickle.dump(data, file)
    with open('prediction.dat', 'w') as file:
        pickle.dump(predict.astype(int), file)
    data = genfromtxt('kaggle_test_wc.csv', skip_header = 1, delimiter = ',')
    data = data[:, 1:]
    with open('test_wordcount.dat', 'w') as file:
        pickle.dump(data, file)

def read_tfidf_file():
    data = genfromtxt('kaggle_train_tf_idf.csv', skip_header = 1, delimiter = ',')
    predict = data[:, -1]
    data = data[:, 1:-1]
    with open('tfidf.dat', 'w') as file:
        pickle.dump(data, file)
    data = genfromtxt('kaggle_test_tf_idf.csv', skip_header = 1, delimiter = ',')
    data = data[:, 1:]
    with open('test_tfidf.dat', 'w') as file:
        pickle.dump(data, file)

class KaggleModelClass:
    def __init__(self, new_data_conf = None, file = ''):
        with open('prediction.dat', 'r') as file:
            self.data_y= pickle.load(file)
        if file == 'wordcount':
            with open('wordcount.dat', 'r') as file:
                self.data_x = pickle.load(file)
            with open('test_wordcount.dat', 'r') as file:
                self.test_data = pickle.load(file)
        else:
            with open('tfidf.dat', 'r') as file:
                self.data_x = pickle.load(file)
            with open('test_tfidf.dat', 'r') as file:
                self.test_data = pickle.load(file)

        # new train data
        if new_data_conf != None:
            new_data = genfromtxt('new_data-%s.txt' % new_data_conf, dtype = int)
            new_data_x = self.test_data[new_data[:, 0]]
            new_data_y = new_data[:, 1]

            self.data_x = vstack((self.data_x, new_data_x))
            self.data_y = hstack((self.data_y, new_data_y))
            #index = permutation(self.data_x.shape[0])
            #for i in range(len(index)):
            #    print index[i]
            #exit()
            index = genfromtxt('permut-%s.txt' % new_data_conf, dtype = int)

            self.data_x = self.data_x[index]
            self.data_y = self.data_y[index]
            print self.data_x.shape
        self.L = len(self.data_y)
        self.N = self.data_x.shape[1]
        
    def setup_cross_validate_data(self, fold = 5):
        test_index = []
        train_data_x_suit, train_data_y_suit = [], []
        test_data_x_suit, test_data_y_suit = [], []
        subL = self.L / fold
        for i in range(fold):
            test_index.append(range(i * subL, (i + 1) * subL))
        for i in range(fold):
            suit = [j for j in range(fold) if j != i]
            train_index = reduce(operator.add, [test_index[j] for j in suit])
            train_data_x_suit.append(self.data_x[train_index, :])
            train_data_y_suit.append(self.data_y[train_index])
            test_data_x_suit.append(self.data_x[test_index[i], :])
            test_data_y_suit.append(self.data_y[test_index[i]])
        self.train_data_x_suit = train_data_x_suit
        self.train_data_y_suit = train_data_y_suit
        self.test_data_x_suit = test_data_x_suit
        self.test_data_y_suit = test_data_y_suit

    def cross_validate(self, fold = 5):
        self.setup_cross_validate_data(fold)

        train_score, test_score = [], []
        for i in range(fold):
            print 'Cross Validation Suit No.%d ...' % i
            self.fit(self.train_data_x_suit[i], self.train_data_y_suit[i])
            result = self.predict(self.train_data_x_suit[i])
            train_score.append(score_func(result, self.train_data_y_suit[i]))
            result = self.predict(self.test_data_x_suit[i])
            test_score.append(score_func(result, self.test_data_y_suit[i]))
            print train_score
            print test_score
        print 'Training Score : ',
        for i in range(fold):
            print '%12f' % train_score[i],
        print
        print 'Testing Score  : ',
        for i in range(fold):
            print '%12f' % test_score[i],
        print
        return train_score, test_score

    def decision_function(self, data_x):
        return self.model.decision_function(data_x)

    def write_submission(self, data_x = None, fname = ''):
        now = datetime.datetime.now()
        header = 'Id,Prediction\n'
        if data_x == None: data_x = self.test_data
        result = self.predict(data_x)

        if fname == '':
            fname = 'submit_%s_%s%d_%s%s.txt' % (
                    self.__str__(),
                    now.strftime('%b'), 
                    now.day, 
                    now.strftime('%H'),
                    now.strftime('%M')
                    )
        with open(fname, 'w') as file:
            file.write(header)
            for i in range(len(result)):
                file.write('%d,%d\n' % (i + 1, result[i]))

    def write_score(self, data_x = None, fname = ''):
        now = datetime.datetime.now()
        header = 'Id,Score\n'
        if data_x == None: data_x = self.test_data
        result = self.decision_function(data_x)
        if fname == '':
            fname = 'score_%s_%s%d_%s%s.txt' % (
                    self.__str__(),
                    now.strftime('%b'), 
                    now.day, 
                    now.strftime('%H'),
                    now.strftime('%M')
                    )
        with open(fname, 'w') as file:
            file.write(header)
            for i in range(len(result)):
                file.write('%d,%f\n' % (i + 1, result[i]))
                
if __name__ == '__main__':
    read_wordcount_file()
    read_tfidf_file()
