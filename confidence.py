#! /usr/bin/env python2.7
from KaggleModelClass import *
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier, GradientBoostingClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import LinearSVC

class Ridge(KaggleModelClass):
    def __init__(self, alpha):
        KaggleModelClass.__init__(self)
        self.model = RidgeClassifier(alpha = alpha)
    def train(self, data, predict):
        self.model.fit(data, predict)
    def test(self, data):
        return self.model.predict(data)
    def score(self, data):
        return self.model.decision_function(data)

class DecisionTree(KaggleModelClass):
    def __init__(self, min_samples_leaf = 2.):
        KaggleModelClass.__init__(self)
        self.model = DecisionTreeClassifier(min_samples_leaf = min_samples_leaf)
    def train(self, data, predict):
        self.model.fit(data, predict)
    def test(self, data):
        return self.model.predict(data)

def cross_confidence(model, data):
    result = model.test(data)
    score = model.score(data)
    L = len(score)
    print L

    index_ordered = argsort(abs(score))[::-1]
    score_ordered = sort(abs(score))[::-1]
    
    incorrect = amap(operator.xor, result[index_ordered], model.predict[index_ordered])
    cum_incorrect = cumsum(incorrect)
    cum_incorrect_reverse = cumsum(incorrect[::-1])
    correct_ratio = 1. * (arange(1, L + 1) - cum_incorrect) / arange(1, L + 1)
    correct_ratio_reverse = 1. * (arange(1, L + 1) - cum_incorrect_reverse) / arange(1, L + 1)
    correct_ratio_reverse = correct_ratio_reverse[::-1]

    F_target = 0.95
    frac = arange(1, L + 1, 1.) / L
    F_low = (F_target - correct_ratio * frac) / (1. - frac)
    F_low[F_low > 1.] = inf
    return score_ordered[argmin(F_low)]
    '''
    plot(score_ordered, correct_ratio, 'b-')
    plot(score_ordered, correct_ratio_reverse, 'b--')
    plot(score_ordered, frac, 'k:')
    plot(score_ordered, 1. - frac, 'k:')
    plot(score_ordered, F_low, 'r')
    xlim((0, 1.5))
    ylim((0.4, 1.01))
    xlabel('score')
    ylabel('ratio (100%)')
    show()
    '''
    #savefig('confidence.png', bbox_inches = 'tight')

if __name__ == '__main__':
    model = Ridge(0.)
    model.train(model.data, model.predict)
    u = cross_confidence(model, model.data)
    print u
