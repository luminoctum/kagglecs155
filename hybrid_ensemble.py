#! /usr/bin/env python2.7
from KaggleModelClass import *
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import RidgeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier, \
        GradientBoostingClassifier, GradientBoostingRegressor, RandomForestClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.grid_search import GridSearchCV
from kaggle import LassoFit

class GradientBoost(KaggleModelClass):
    def __init__(self, 
            max_depth = 4, 
            n_estimators = 5000, 
            learning_rate = 0.05,
            subsample = 0.55
            ):
        KaggleModelClass.__init__(self)
        self.model = GradientBoostingClassifier(
                max_depth = max_depth,
                n_estimators = n_estimators,
                learning_rate = learning_rate)
    def fit(self, data_x, data_y):
        self.model.fit(data_x, data_y)

    def predict(self, data_x):
        return self.model.predict(data_x)

    def __str__(self):
        return 'GradientBoost'

class Bagging(KaggleModelClass):
    def __init__(self, model, n_estimators):
        KaggleModelClass.__init__(self)
        bag_model = BaggingClassifier(
                base_estimator = model,
                n_estimators = 100)
        self.model = bag_model
        
    def fit(self, data, predict):
        self.model.fit(data, predict)

    def predict(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Bagging'

class RandomForest(KaggleModelClass):
    def __init__(self, n_estimators, min_samples_leaf):
        KaggleModelClass.__init__(self)
        model = RandomForestClassifier(
                n_estimators = n_estimators,
                max_depth = min_samples_leaf)
        self.model = model
        
    def fit(self, data, predict):
        self.model.fit(data, predict)

    def predict(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Bagging'

class HybridEnsemble(KaggleModelClass):
    def __init__(self, 
            linear_model = None, 
            aux_model = None, 
            u = 0.5, 
            r = -1, 
            new_data = None, 
            name = ''):
        KaggleModelClass.__init__(self, new_data)
        self.linear_model = linear_model
        self.aux_model = aux_model
        self.u = u
        self.r = r
        self.name = name

    def fit(self, data_x, data_y):
        self.linear_model.fit(data_x, data_y)
        linear_result= self.linear_model.predict(data_x)
        score = self.linear_model.decision_function(data_x)
        L = len(score)
        index_1 = arange(L)[abs(score) > self.u]
        index_2 = arange(L)[abs(score) <= self.u]
        if (self.r > 0):
            index_t2 = arange(L)[abs(score) <= self.r * self.u]
            self.aux_model.fit(data_x[index_t2], data_y[index_t2])
        else:
            self.aux_model.fit(data_x, data_y)

        return index_1, index_2
        
    def predict(self, data_x, return_index = False):
        result = zeros(len(data_x), dtype = int)
        score = self.linear_model.decision_function(data_x)
        L = len(score)
        index_1 = arange(L)[abs(score) > self.u]
        index_2 = arange(L)[abs(score) <= self.u]

        result[index_1] = self.linear_model.predict(data_x[index_1])
        result[index_2] = self.aux_model.predict(data_x[index_2])
        if return_index:
            return result, index_1, index_2
        else:
            return result

    def cross_validate(self, fold = 5):
        self.setup_cross_validate_data(fold)

        score_list, score, cross_score_list, cross_score = [], zeros(6), [], zeros(4)
        for i in range(fold):
            print 'Cross Validation Suit No.%d ...' % i
            data_x = self.train_data_x_suit[i]
            data_y = self.train_data_y_suit[i]
            index_1, index_2 = self.fit(data_x, data_y)
            score[0] = score_func(self.linear_model.predict(data_x[index_1]), data_y[index_1])
            cross_score[0] = score_func(self.linear_model.predict(data_x[index_2]), data_y[index_2])
            score[1] = score_func(self.aux_model.predict(data_x[index_2]), data_y[index_2])
            cross_score[1] = score_func(self.aux_model.predict(data_x[index_1]), data_y[index_1])
            result = self.predict(data_x)
            score[2] = score_func(result, data_y)

            data_x = self.test_data_x_suit[i]
            data_y = self.test_data_y_suit[i]
            result, index_1, index_2 = self.predict(data_x, return_index = True)
            score[3] = score_func(result[index_1], data_y[index_1])
            cross_score[2] = score_func(self.linear_model.predict(data_x[index_2]), data_y[index_2])
            score[4] = score_func(result[index_2], data_y[index_2])
            cross_score[3] = score_func(self.aux_model.predict(data_x[index_1]), data_y[index_1])
            score[5] = score_func(result, data_y)
            score_list.append(score.copy())
            cross_score_list.append(cross_score.copy())
            print score[2], score[5]

        print 'Linear model : ',
        for i in range(fold):
            print '%10f/%-10f' % (score_list[i][0], score_list[i][3]),
        print
        print '       cross : ',
        for i in range(fold):
            print '%10f/%-10f' % (cross_score_list[i][0], cross_score_list[i][2]),
        print
        print 'Aux model    : ',
        for i in range(fold):
            print '%10f/%-10f' % (score_list[i][1], score_list[i][4]),
        print
        print '       cross : ',
        for i in range(fold):
            print '%10f/%-10f' % (cross_score_list[i][1], cross_score_list[i][3]),
        print
        print 'Total        : ',
        for i in range(fold):
            print '%10f/%-10f' % (score_list[i][2], score_list[i][5]),
        print
        avg_train, avg_test = 0., 0.
        for i in range(fold):
            avg_train += score_list[i][2]
            avg_test += score_list[i][5]
        avg_train /= fold
        avg_test /= fold
        print 'Average      :  %10f/%-10f' % (avg_train, avg_test)

    def __str__(self):
        return 'he_' + self.name

def simple_model_test():
    base = KaggleModelClass()

    model = RidgeClassifier(
            alpha = 0)

    model = GradientBoostingClassifier(
            max_depth = 4,
            n_estimators = 10,
            learning_rate = 0.03)

    model = BaggingClassifier(
            base_estimator = model,
            n_estimators = 2)

    print cross_validation.cross_val_score(model, base.data_x, base.data_y, cv = 5, verbose = 2)

def complex_model_test(kind = '', params = []):
    name = reduce(lambda a, b: str(a) + '_' + str(b), params)

    linear_model = RidgeClassifier(
            alpha = 0.)

    aux_model = GradientBoostingClassifier(
            max_depth = params[0],
            n_estimators = params[1],
            learning_rate = params[2],
            subsample = params[3])

    bag_model = BaggingClassifier(
            base_estimator = aux_model,
            n_estimators = 20,
            n_jobs = 10)

    model = HybridEnsemble(
            linear_model = linear_model, 
            aux_model = aux_model, 
            u = params[4], 
            r = params[5], 
            new_data = params[6],
            name = name)

    print 'parameters :'
    print 'max_depth = '        , params[0]
    print 'n_estimators = '     , params[1]
    print 'learning_rate = '    , params[2]
    print 'subsample = '        , params[3]
    print 'u = '                , params[4]
    print 'r = '                , params[5]
    print 'new_data = '         , params[6]
    print linear_model
    print aux_model

    if kind == 'cross_validate':
        model.cross_validate(5)
    else:
        model.fit(model.data_x, model.data_y)
        model.write_submission()

if __name__ == '__main__':
    #simple_model_test()
    # model parameter
    # max_depth, n_estimators, learning_rate, subsample, u, r, new_data
    #params = [4, 400, 0.03, 0.5, 0.8, 0.98]
    params = [4, 3000, 0.01, 1.0, 0.5, 1.6, 0.98]
    model = GradientBoost()
    model.cross_validate()
    #complex_model_test(kind = 'cross_validate', params = params)
    #complex_model_test(kind = 'submit', params = params)
