#! /usr/bin/env python2.7
from KaggleModelClass import *
from sklearn import linear_model, svm, tree
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, BaggingClassifier, GradientBoostingClassifier
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

class LassoFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.LassoLars(alpha = 1.E-6)
        #model = linear_model.Lasso(alpha = 1.E-6)
        model.fit(data, predict)
        id = where(model.coef_ != 0)[0]

        model = linear_model.RidgeClassifier(alpha = 0.E-3)
        model.fit(data[:, id], predict)
        self.model = model
        self.id = id

    def test(self, data):
        result = self.model.predict(data[:, self.id])
        return result

    def __str__(self):
        return 'Lasso'

class RidgeFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.RidgeClassifier(alpha = 0.E-3)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Ridge'

class BayesianRidgeFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.BayesianRidge()
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        result[result > 0.5] = 1
        result[result < 0.5] = 0
        result = amap(int, result)
        return result

    def __str__(self):
        return 'BayesianRidgeFit'

class LogisticRegressionFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.LogisticRegression(C = 1.E5)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def score(self, data):
        return self.model.decision_function(data)

    def __str__(self):
        return 'LogisticRegression'

class Svc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.SVC(C = 1)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'SVC'

class NuSvc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.NuSVC(nu = 0.01)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'NuSVC'

class LinearSvc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.LinearSVC(C = 1.E7)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'LinearSVC'

class DecisionTree(KaggleModelClass):
    def train(self, data, predict):
        model = tree.DecisionTreeClassifier(max_features = 300, min_samples_split = 2)
        #model = tree.DecisionTreeClassifier()
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'DecisionTree'

class AdaBoost(KaggleModelClass):
    def train(self, data, predict):
        model = AdaBoostClassifier(n_estimators = 200)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'AdaBoost'

class RandomForest(KaggleModelClass):
    def train(self, data, predict):
        model = RandomForestClassifier(n_estimators = 1000, max_depth = 50)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'RandomForest'

class Bagging(KaggleModelClass):
    def train(self, data, predict):
        model = BaggingClassifier(base_estimator = tree.DecisionTreeClassifier(max_depth = 6),
                n_estimators = 10)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Bagging'

class Hybrid(KaggleModelClass):
    def __init__(self, model_2, u):
        KaggleModelClass.__init__(self)
        self.model_1 = linear_model.RidgeClassifier(alpha = 0.)
        self.model_2 = model_2
        self.u = u
        #self.model = model_1

    def train(self, data, predict):
        self.model_1.fit(data, predict)
        score = self.model_1.decision_function(data)
        L = len(score)
        index_1 = arange(L)[abs(score) > self.u]
        index_2 = arange(L)[abs(score) <= self.u]
        #data_remain = data[index_2]
        #predict_remain = predict[index_2]
        data_remain = data
        predict_remain = predict
        self.model_2.fit(data_remain, predict_remain)
        print 'training score #1: ', score_func(self.model_1.predict(data[index_1]), predict[index_1])
        print 'training score #2: ', score_func(self.model_2.predict(data[index_2]), predict[index_2])

    def test(self, data):
        result = zeros(len(data), dtype = int)
        score = self.model_1.decision_function(data)
        L = len(score)
        index_1 = arange(L)[abs(score) > self.u]
        index_2 = arange(L)[abs(score) <= self.u]
        result[index_1] = self.model_1.predict(data[index_1])
        result[index_2] = self.model_2.predict(data[index_2])

        return result

    def test2(self, data, predict):
        result = zeros(len(data), dtype = int)
        score = self.model_1.decision_function(data)
        L = len(score)
        index_1 = arange(L)[abs(score) > self.u]
        index_2 = arange(L)[abs(score) <= self.u]
        result[index_1] = self.model_1.predict(data[index_1])
        result[index_2] = self.model_2.predict(data[index_2])
        print 'testing score #1: ', score_func(self.model_1.predict(data[index_1]), predict[index_1])
        print 'testing score #2: ', score_func(self.model_2.predict(data[index_2]), predict[index_2])
        return result

    def __str__(self):
        return 'HybridGradientBoost'

if __name__ == '__main__':
    #model = LassoFit()
    #model = RidgeFit()
    #model = BayesianRidgeFit()
    #model = AdaBoost()
    #model = LogisticRegressionFit()
    #model = Svc()
    #model = AdaBoostClassifier(
    #        base_estimator = tree.DecisionTreeClassifier(max_depth = 1),
    #        n_estimators = 500)
    model = GradientBoostingClassifier(
            n_estimators = 500,
            learning_rate = 0.05,
            max_depth = 2)
    #model = DecisionTreeClassifier(
    #        min_samples_leaf = 5)
    # u = 0.4 and u = 0.5 both give 0.93925
    model = Hybrid(model, u = 0.5)
    #model = DecisionTree()
    #model = RandomForest()
    #model = Bagging()
    model.cross_validate()
    #model.train(model.data, model.predict)
    #model.write_score(model.data)
    #model.write_submission()
