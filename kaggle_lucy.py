#! /usr/bin/env python2.7
from KaggleModelClass import *
from sklearn import linear_model, svm, tree
from sklearn.ensemble import RandomForestClassifier

class LassoFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.LassoLars(alpha = 1.E-6)
        #model = linear_model.Lasso(alpha = 1.E-6)
        model.fit(data, predict)
        id = where(model.coef_ != 0)[0]

        coef, junk, junk, junk = lstsq(data[:, id], predict)
        model.coef_[id] = coef
        self.coef = model.coef_

    def test(self, data):
        # testing score
        result = dot(data, self.coef)
        result[result > 0.5] = 1
        result[result < 0.5] = 0
        # result has to be int
        result = amap(int, result)
        return result

    def __str__(self):
        return 'Lasso'

class RidgeFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.RidgeClassifier(alpha = 0.E-5)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Ridge'

class BayesianRidgeFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.BayesianRidge()
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        result[result > 0.5] = 1
        result[result < 0.5] = 0
        result = amap(int, result)
        return result

    def __str__(self):
        return 'BayesianRidgeFit'

class LogisticRegressionFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.LogisticRegression(C = 1.E5)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'LogisticRegression'

class Svc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.SVC(C = 10)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'SVC'

class NuSvc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.NuSVC(nu = 0.01)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'NuSVC'

class LinearSvc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.LinearSVC(C = 1.E8)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'LinearSVC'

class DecisionTree(KaggleModelClass):
    def train(self, data, predict):
        model = tree.DecisionTreeClassifier(max_depth = 7)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'DecisionTree'

class RandomForest(KaggleModelClass):
    def train(self, data, predict):
        model = RandomForestClassifier(n_estimators = 20, max_depth = 9)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'RandomForest'

if __name__ == '__main__':
    model = LassoFit()
    #model = RidgeFit()
    #model = BayesianRidgeFit()
    #model = LogisticRegressionFit()
    #model = DecisionTree()
    #model = RandomForest()
    model.cross_validate()
    #model.train(model.data, model.predict)
    #model.write_submission()
