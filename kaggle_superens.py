#! /usr/bin/env python2.7
from KaggleModelClass import *
from sklearn import linear_model, svm, tree
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, BaggingClassifier, GradientBoostingClassifier
from sklearn.neighbors import NearestCentroid, KNeighborsClassifier, RadiusNeighborsClassifier

class SuperEnsemble(KaggleModelClass):
    def train_ensemble(self, data, predict):
        #Set up the model ensemble
        ensemble = []
        #
        #Linear Classifiers
        # ensemble.append(linear_model.RidgeClassifier(alpha = 0))
        # ensemble.append(linear_model.RidgeClassifier(alpha = 1))
        # ensemble.append(linear_model.RidgeClassifier(alpha = 2))
        #
        #Support Vector Classifiers
        # ensemble.append(svm.SVC(C = 1.E-4))
        # ensemble.append(svm.SVC(C = 1.E-0))
        # ensemble.append(svm.SVC(C = 1.E+4))
        # ensemble.append(svm.SVC(C = 1.E-4, kernel='linear', class_weight='auto'))
        # ensemble.append(svm.SVC(C = 1.E-0, kernel='linear', class_weight='auto'))
        # ensemble.append(svm.SVC(C = 1.E+4, kernel='linear', class_weight='auto'))
        # ensemble.append(svm.SVC(C = 1.E-4, kernel='poly', class_weight='auto'))
        # ensemble.append(svm.SVC(C = 1.E-0, kernel='poly', class_weight='auto'))
        # ensemble.append(svm.SVC(C = 1.E+4, kernel='poly', class_weight='auto'))
        #
        # #Decision tree classifiers
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=1))
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=2))
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=3))
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=4))
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=5))
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=6))
        # ensemble.append(tree.DecisionTreeClassifier(max_depth=7))
        # ensemble.append(tree.DecisionTreeClassifier(min_samples_leaf=16))
        # ensemble.append(tree.DecisionTreeClassifier(min_samples_leaf=32))
        # ensemble.append(tree.DecisionTreeClassifier(min_samples_leaf=64))
        # ensemble.append(tree.DecisionTreeClassifier(min_samples_leaf=128))

        # #Bagged tree classifiers
        # ensemble.append(BaggingClassifier(n_estimators = 35, n_jobs=-1))

        # #Boosted tree classifiers
        # ensemble.append(AdaBoostClassifier(n_estimators = 100))
        ensemble.append(GradientBoostingClassifier(n_estimators = 400, max_depth=4, learning_rate=0.03))
        #
        # #NN methods
        # ensemble.append(KNeighborsClassifier(n_neighbors = 2))
        # ensemble.append(KNeighborsClassifier(n_neighbors = 4))
        # ensemble.append(KNeighborsClassifier(n_neighbors = 6))

        #Train the models individually
        for models in ensemble:
             models.fit(data, predict)

        self.ensemble = ensemble

    def train(self, data, predict):
        ensemble_best = []

        ensemble_prediction = zeros(len(data)).astype(int)
        num_ens = 1 #Number of models to be selected for best ensemble

        #This loop selects the model added to the best ensemble
        for i in range(num_ens):
            ensemble_score = []

            #Test avg prediction for all models in ensemble
            for models in self.ensemble:
                ensemble_test = self.ens_avg(ensemble_prediction, models.predict(data))
                ensemble_score.append(score_func(ensemble_test, predict))

            #Add best new model to ensemble prediction via averaging
            max_ind = np.argmax(ensemble_score)
            if score_func(ensemble_prediction, predict) < ensemble_score[max_ind]:
                ensemble_prediction = self.ens_avg(ensemble_prediction, self.ensemble[max_ind].predict(data))
                ensemble_best.append(self.ensemble[max_ind])
            else:
                break

        self.ensemble_best = ensemble_best

    def test(self, data):
        result = zeros(len(data))

        #Get the average classification for the best ensemble
        for models in self.ensemble_best:
             result = self.ens_avg(result, models.predict(data))

        return result

    def ens_avg(self, model1, model2):
        result = 2*(model1 + model2) - 2
        result = np.tanh(result) >= 0
        return result.astype(int)

    def cross_validate(self):
        train_score, test_score = [], []
        for i in range(self.fold):
            print 'Cross Validation Suit No.%d ...' % i
            self.train_ensemble(self.train_data_suit[i], self.train_predict_suit[i])
            self.train(self.test_data_suit[i], self.test_predict_suit[i])
            result = self.test(self.train_data_suit[i])
            train_score.append(score_func(result, self.train_predict_suit[i]))
            print train_score
            result = self.test(self.test_data_suit[i])
            test_score.append(score_func(result, self.test_predict_suit[i]))
            print test_score
        print 'Training Score : ',
        for i in range(self.fold):
            print '%12f' % train_score[i],
        print
        print 'Testing Score  : ',
        for i in range(self.fold):
            print '%12f' % test_score[i],
        print
        return train_score, test_score

    def fit(self, data, predict):
        self.train_ensemble(data, predict)
        return self.train(data, predict)

    def predict(self, data):
        return self.test(data)

    def __str__(self):
        return 'SuperEnsemble'

if __name__ == '__main__':
    model = SuperEnsemble()
    #model.cross_validate()
    model.train_ensemble(model.data, model.predict)
    model.train(model.data, model.predict)
    #model.write_score(model.data)
    model.write_submission()
