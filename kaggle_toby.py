#! /usr/bin/env python2.7
from KaggleModelClass import *
from sklearn import linear_model, svm, tree
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, BaggingClassifier, GradientBoostingClassifier
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.grid_search import GridSearchCV

class LassoFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.LassoLars(alpha = 1.E-6)
        #model = linear_model.Lasso(alpha = 1.E-6)
        model.fit(data, predict)
        id = where(model.coef_ != 0)[0]

        model = linear_model.RidgeClassifier(alpha = 0.E-3)
        model.fit(data[:, id], predict)
        self.model = model
        self.id = id

    def test(self, data):
        result = self.model.predict(data[:, self.id])
        return result

    def __str__(self):
        return 'Lasso'

class RidgeFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.RidgeClassifier(alpha = 0.E-3)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Ridge'

class BayesianRidgeFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.BayesianRidge()
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        result[result > 0.5] = 1
        result[result < 0.5] = 0
        result = amap(int, result)
        return result

    def __str__(self):
        return 'BayesianRidgeFit'

class LogisticRegressionFit(KaggleModelClass):
    def train(self, data, predict):
        model = linear_model.LogisticRegression(C = 1.E5)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def score(self, data):
        return self.model.decision_function(data)

    def __str__(self):
        return 'LogisticRegression'

class Svc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.SVC(C = 1)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'SVC'

class NuSvc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.NuSVC(nu = 0.01)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'NuSVC'

class LinearSvc(KaggleModelClass):
    def train(self, data, predict):
        model = svm.LinearSVC(C = 1.E7)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'LinearSVC'

class DecisionTree(KaggleModelClass):
    def train(self, data, predict):
        model = tree.DecisionTreeClassifier(max_features = 300, min_samples_split = 2)
        #model = tree.DecisionTreeClassifier()
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'DecisionTree'

class AdaBoost(KaggleModelClass):
    def train(self, data, predict):
        model = AdaBoostClassifier(n_estimators = 200)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'AdaBoost'

class GradientBoosting(KaggleModelClass):
    def train(self, data, predict):
        model = GradientBoostingClassifier(n_estimators = 1)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def search(self):
        grid = {'max_depth': [1, 2, 4, 8],
                'min_samples_leaf': [1, 2, 4, 8],
                'max_features': [1.0, 0.5, 0.25]}
        model = GradientBoostingClassifier(n_estimators = 100, subsample=0.5)
        gs_cv = GridSearchCV(estimator=model, param_grid=grid, cv=5, n_jobs=-1).fit(self.data, self.predict)
        print gs_cv.best_params_
        print gs_cv.best_score_

    def __str__(self):
        return 'GradientBoosting'

class Bagging(KaggleModelClass):
    def train(self, data, predict):
        model = BaggingClassifier(n_estimators=10)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'Bagging'

class RandomForest(KaggleModelClass):
    def train(self, data, predict):
        model = RandomForestClassifier(n_estimators = 1000, max_depth = 50)
        model.fit(data, predict)
        self.model = model

    def test(self, data):
        result = self.model.predict(data)
        return result

    def __str__(self):
        return 'RandomForest'

class Hybrid(KaggleModelClass):
    def train(self, data, predict):
        #model_1 = linear_model.LogisticRegression(C = 1.E5)
        model_1 = linear_model.RidgeClassifier(alpha = 0.)
        model_1.fit(data, predict)
        score = model_1.decision_function(data)
        L = len(score)
        sort_index = argsort(abs(score))[::-1]
        index_1 = sort_index[:3*L/4]
        index_2 = sort_index[3*L/4:]
        self.model_1 = model_1
        self.model = model_1

        data_remain = data[index_2, :]
        #predict_remain = predict[index_2,:]
        predict_remain = predict[ix_(index_2)]
        model_2 = tree.DecisionTreeClassifier(max_depth = 6)
        model_2.fit(data_remain, predict_remain)
        self.model_2 = model_2

        model_3 = AdaBoostClassifier(n_estimators = 100)
        model_3.fit(data_remain, predict_remain)
        self.model_3 = model_3

    def test(self, data):
        result = zeros(len(data), dtype = int)
        score = self.model_1.decision_function(data)
        L = len(score)
        sort_index = argsort(abs(score))[::-1]
        index_1 = sort_index[:3*L/4]
        index_2 = sort_index[3*L/4:]
        result[index_1] = self.model_1.predict(data[index_1])
        #result[index_2] = self.model_2.predict(data[index_2])
        result_a = self.model_1.predict(data[index_2])
        result_b = self.model_2.predict(data[index_2])
        result_c = self.model_3.predict(data[index_2])
        print result_a
        #print result_b
        #print result_c
        result[index_2] = [int(round((result_a[i] + result_b[i] + result_c[i]) / 3.)) for i in range(len(index_2))]

        return result

    def __str__(self):
        return 'HybridAdaBoost'

class Blending(KaggleModelClass):
    def train(self, data, predict):
        #Set up the model ensemble
        model = []
        model.append(linear_model.RidgeClassifier(alpha = 0.))
        model.append(tree.DecisionTreeClassifier(max_depth=9))
        model.append(AdaBoostClassifier(n_estimators = 100))
        model.append(BaggingClassifier(n_estimators = 30, n_jobs=-1))
        model.append(GradientBoostingClassifier(n_estimators = 400, max_depth=4, learning_rate=0.03))

        #Train the models individually
        for models in model:
             models.fit(data, predict)

        #Get the ensemble matrix for reg least squares
        e_mat = np.zeros((len(data), len(model)))
        for i in range(1,len(model)):
            e_mat[:,i] = model[i].predict(data)

        #Get the weight vector using regularized least squares
        lamb = 0.01
        lhs = np.dot(e_mat.T, e_mat) + lamb
        rhs = np.dot(e_mat.T, predict)
        w = np.linalg.solve(lhs, rhs)

        self.model = model
        self.weights = w

    def test(self, data):
        result = zeros(len(data))

        for i in range(1, len(self.model)):
            result += self.weights[i]*(2*self.model[i].predict(data) - 1)

        result = np.tanh(result) > 0
        result = result.astype(int)

        return result

    def __str__(self):
        return 'Blending'

if __name__ == '__main__':
    #model = LassoFit()
    #model = RidgeFit()
    #model = BayesianRidgeFit()
    #model = AdaBoost()
    model = GradientBoosting()
    #model = Bagging()
    #model = LogisticRegressionFit()
    #model = Svc()
    #model = DecisionTree()
    #model = RandomForest()
    #model = Hybrid()
    #model = Blending()
    #model = DecisionTree()
    #model = RandomForest()
    #model.cross_validate()
    model.search()
    #model.train(model.data, model.predict)
    #model.write_score(model.data)
    #model.write_submission()
