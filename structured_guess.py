#! /usr/bin/env python2.7
from pylab import *
import operator, pickle

def divide_sequence(test0, cur, correct, seglen, ratio, score_func, fraction):
    test = list(test0)
    seglen.insert(cur, int(round(fraction * seglen[cur])))
    seglen[cur + 1] = seglen[cur + 1] - seglen[cur]
    shift = sum(seglen[:cur + 1])
    for i in range(seglen[cur + 1]) : test[i + shift] = int(not test[i + shift])
    score = score_func(test)
    old_correct = correct[cur]
    new_correct = score - sum(correct) + correct[cur]
    correct.insert(cur, (old_correct + new_correct - seglen[cur + 1]) / 2)
    correct[cur + 1] = correct[cur + 1] - correct[cur]
    if (correct[cur] > seglen[cur] or correct[cur + 1] > seglen[cur + 1]):
        print cur, seglen, correct, 'error'
        exit()
    ratio.insert(cur, float(correct[cur]) / (seglen[cur]))
    ratio[cur + 1] = float(correct[cur + 1]) / (seglen[cur + 1])

def sort_predict_by_score(fname):
    submit_file = 'submit_' + fname + '.txt'
    score_file = 'score_' + fname + '.txt'
    predict = genfromtxt(submit_file, skip_header = 1, delimiter = ',', usecols = (1,), dtype = int)
    score = genfromtxt(score_file, skip_header = 1, delimiter = ',', usecols = (1,))
    predict = predict[:4000]
    score = score[:4000]
    #x = sort(abs(score))[::-1]
    #for i in range(len(x)):
    #    print i,x[i]
    #exit()
    sort_index = argsort(abs(score))[::-1]
    return predict[sort_index], sort_index

def structured_guess(num, score):
    test, sort_index = sort_predict_by_score('structured_Feb7_1322_0')
    L = len(test)
    if num == 0:
        cur, correct, ratio, seglen, it = 0, [score], [float(score)/L], [L], 0
        gain, gain_list = 0, [0]
    else:
        with open('guess_sequence_%d.dat' % (num - 1,), 'r') as file:
            cur = pickle.load(file)
            correct = pickle.load(file)
            ratio = pickle.load(file)
            seglen = pickle.load(file)
            it = pickle.load(file)
            gain = pickle.load(file)
            gain_list = pickle.load(file)
        old_correct = correct[cur]
        new_correct = score - sum(correct) + correct[cur]
        correct.insert(cur, (old_correct + new_correct - seglen[cur + 1]) / 2)
        correct[cur + 1] = correct[cur + 1] - correct[cur]
        if (correct[cur] > seglen[cur] or correct[cur + 1] > seglen[cur + 1]):
            print cur, seglen, correct, 'error'
            exit()
        ratio.insert(cur, float(correct[cur]) / (seglen[cur]))
        ratio[cur + 1] = float(correct[cur + 1]) / (seglen[cur + 1])
        cur = argmin(ratio)
        if correct[cur] == seglen[cur] :
            gain += seglen[cur]
        if correct[cur] == 0 :
            ratio[cur] = 1.
            gain += seglen[cur]
        gain_list.append(gain)
        for cur in range(len(ratio) - 1, -1, -1):
            if ratio[cur] != 1.: break
        #cur = argmin(ratio)
        #cur = 27
        print 'cur = ', cur
        print 'score   = %f' % (float(score) / L,)
        print 'seglen  = ', seglen
        print 'correct = ', correct
        print 'ratio   = ', ratio
        print '====='

    seglen.insert(cur, int(round(0.5 * seglen[cur])))
    seglen[cur + 1] = seglen[cur + 1] - seglen[cur]
    with open('guess_sequence_%d.dat' % num, 'w') as file:
        pickle.dump(cur, file)
        pickle.dump(correct, file)
        pickle.dump(ratio, file)
        pickle.dump(seglen, file)
        pickle.dump(it, file)
        pickle.dump(gain, file)
        pickle.dump(gain_list, file)

    shift = sum(seglen[:cur + 1])
    for i in range(seglen[cur + 1]) : test[i + shift] = int(not test[i + shift])
    new_test = zeros(L)
    for i in range(L):
        new_test[sort_index[i]] = test[i]
    create_submission(new_test, 'submit_structured_Feb7_1322_%d.txt' % (num + 1, ))

def create_submission(test, fname):
    predict = genfromtxt('submit_structured_Feb7_1322_0.txt', 
        skip_header = 1, delimiter = ',', usecols = (1,), dtype = int)
    header = 'Id,Prediction\n'
    with open(fname, 'w') as file:
        file.write(header)
        for i in range(len(test)):
            file.write('%d,%d\n' % (i + 1, test[i]))
        for i in range(len(test), len(predict)):
            file.write('%d,%d\n' % (i + 1, predict[i]))

def reconstruct():
    seglen = array([125, 63, 62, 125, 63, 62, 500, 125, 125, 250, 500, 500, 500, 500, 250, 125, 
            63, 31, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 4, 1, 1, 1])
    cumseglen = cumsum(seglen)
    correct = array([125, 63, 61, 125, 63, 61, 492, 124, 125, 248, 489, 482, 463, 398, 169, 87, 
            39, 18, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1])
    ratio = 1. * correct / seglen
    ratio[ratio == 0.] = 1.

    cumseglen = hstack((0, cumseglen))
    L = len(seglen)
    test, sort_index = sort_predict_by_score('structured_Feb7_1322_0')
    threshold, id, val, n_incorrect = 0.98, array([], dtype = int), array([], dtype = int), 0
    for i in range(L):
        if ratio[i] >= threshold:
            id = hstack((id, sort_index[range(cumseglen[i], cumseglen[i + 1])]))
            if correct[i] > 0:
                val = hstack((val, test[range(cumseglen[i], cumseglen[i + 1])]))
            else:
                n_incorrect += seglen[i]
                incorrect = test[range(cumseglen[i], cumseglen[i + 1])]
                val = hstack((val, amap(operator.not_, incorrect)))
    print id
    print val
    print len(id)
    print n_incorrect

    data_file = 'new_data_0.98.txt'
    with open(data_file, 'w') as file:
        for i in range(len(id)):
            file.write('%d %d\n' % (id[i], val[i]))

'''
    predict = genfromtxt('submit_structured_Feb7_1322_0.txt', 
        skip_header = 1, delimiter = ',', usecols = (1,), dtype = int)
    
    predict[id] = val
    fname = 'sub_benchmark_42.txt'
    header = 'Id,Prediction\n'
    with open(fname, 'w') as file:
        file.write(header)
        for i in range(len(predict)):
            file.write('%d,%d\n' % (i + 1, predict[i]))
'''


if __name__ == '__main__':
    #synthetic_structured_guess()
    #structured_guess(0, 3725)
    #structured_guess(1, 2307)
    #structured_guess(2, 2673)
    #structured_guess(3, 3161)
    #structured_guess(4, 3397)
    #structured_guess(5, 3522)
    #structured_guess(6, 3522)
    #structured_guess(7, 3167)
    #structured_guess(8, 3197)
    #structured_guess(9, 3493)
    #structured_guess(10, 3219)
    #structured_guess(11, 3399)
    #structured_guess(12, 3520)
    #structured_guess(13, 3585)
    #structured_guess(14, 3585)
    #structured_guess(15, 3581)
    #structured_guess(16, 3630)
    #structured_guess(17, 3645)
    #structured_guess(18, 3650)
    #structured_guess(19, 3652)
    #structured_guess(20, 3648)
    #structured_guess(21, 3647)
    #structured_guess(22, 3647)
    #structured_guess(23, 3645)
    #structured_guess(24, 3644)
    #structured_guess(25, 3646)
    #structured_guess(26, 3644)
    #structured_guess(27, 3643)
    #structured_guess(28, 3644)
    #structured_guess(29, 3644)
    #structured_guess(30, 3643)
    #structured_guess(31, 3645)
    #structured_guess(32, 3644)
    #structured_guess(33, 3645)
    #structured_guess(34, 3646)
    #structured_guess(35, 3646)
    #structured_guess(36, 3645)
    #structured_guess(37, 3645)
    #structured_guess(38, 3646)
    #structured_guess(39, 3646)
    #structured_guess(40, 3645)
    #structured_guess(41, 3646)
    #structured_guess(42, 3646)
    reconstruct()
